package com.otoniel.qrscanner.ui.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.zxing.Result;
import com.otoniel.qrscanner.activity.MainActivity;
import com.otoniel.qrscanner.databinding.FragmentQrGenerateBinding;
import com.otoniel.qrscanner.databinding.FragmentQrScannerBinding;
import com.otoniel.qrscanner.ui.dialog.LoadingDialog;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class QRScannerFragment extends Fragment implements ZXingScannerView.ResultHandler {

    private static final String TAG = "QRScannerFragment";

    FragmentQrScannerBinding binding;

    private FirebaseAuth mAuth;
    private FirebaseDatabase database;
    private String qrToken;
    private MainActivity activity;
    private LoadingDialog loadingDialog;
    private ZXingScannerView mScannerView;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        activity = (MainActivity) getActivity();
        loadingDialog = new LoadingDialog(activity);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentQrScannerBinding.inflate(inflater, container,false);

        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();

        mScannerView = new ZXingScannerView(activity);

        //return binding.getRoot();
        return mScannerView;
    }

    @Override
    public void onResume() {
        super.onResume();
        loadView();
        getQRCurrent();
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
    }

    @Override
    public void onPause() {
        super.onPause();
        stopScanner();
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = (MainActivity) activity;
    }

    private void loadView() {
        if (binding != null) {
            initScanner();
        }
    }

    private void initScanner() {
        //binding.contentScan.addView(mScannerView);
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    private void stopScanner() {
        mScannerView.stopCamera();
    }

    private void getQRCurrent() {
        loadingDialog.show();
        // [START write_message]
        // Write a message to the database
        DatabaseReference myRef = database.getReference("token_qr");

        //myRef.setValue("Hello, World!");
        // [END write_message]

        // [START read_message]
        // Read from the database
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                loadingDialog.dismiss();
                qrToken = dataSnapshot.getValue(String.class);
                Log.d(TAG, "Value is: " + qrToken);

            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                loadingDialog.dismiss();
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });
        // [END read_message]
    }

    @Override
    public void handleResult(Result rawResult) {
        if (qrToken != null && rawResult.getText().equals(qrToken)) {
            Toast.makeText(activity, "Excelente tienes acceso", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(activity, "El QR escaneado no es correcto", Toast.LENGTH_LONG).show();
        }
        //Toast.makeText(getActivity(), "Contents = " + rawResult.getText() + ", Format = " + rawResult.getBarcodeFormat().toString(), Toast.LENGTH_SHORT).show();
        // Note:
        // * Wait 2 seconds to resume the preview.
        // * On older devices continuously stopping and resuming camera preview can result in freezing the app.
        // * I don't know why this is the case but I don't have the time to figure out.
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mScannerView.resumeCameraPreview(QRScannerFragment.this);
            }
        }, 2000);
    }
}
