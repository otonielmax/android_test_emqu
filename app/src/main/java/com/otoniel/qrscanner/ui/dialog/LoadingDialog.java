package com.otoniel.qrscanner.ui.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.PorterDuff;
import android.view.Window;

import androidx.core.content.ContextCompat;

import com.otoniel.qrscanner.R;

public class LoadingDialog {

    Activity activity;
    Dialog dialog;

    public LoadingDialog(Activity activity) {
        this.activity = activity;
        dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_loading);
        dialog.getWindow().getDecorView().setBackgroundColor(activity.getResources().getColor(R.color.transparent));
        dialog.getWindow().getDecorView().setAlpha(0.9f);
        dialog.findViewById(R.id.progress_bar).setBackgroundTintMode(PorterDuff.Mode.ADD);
        dialog.findViewById(R.id.progress_bar).setBackgroundTintList(ContextCompat.getColorStateList(activity,R.color.colorPrimaryDark));
    }

    public void setOnCancelListener(final DialogInterface.OnCancelListener listener){
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                listener.onCancel(dialog);
            }
        });
    }

    public void show(){
        dialog.show();
    }

    public void dismiss(){
        dialog.dismiss();
    }
}
