package com.otoniel.qrscanner.ui.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.otoniel.qrscanner.activity.LoginActivity;
import com.otoniel.qrscanner.activity.MainActivity;
import com.otoniel.qrscanner.databinding.FragmentLoginBinding;
import com.otoniel.qrscanner.databinding.FragmentRegisterBinding;
import com.otoniel.qrscanner.ui.dialog.LoadingDialog;

public class RegisterFragment extends Fragment {

    private static final String TAG = "RegisterFragment";

    FragmentRegisterBinding binding;

    private FirebaseAuth mAuth;
    private String mCustomToken;
    private LoginActivity activity;
    private LoadingDialog loadingDialog;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        activity = (LoginActivity) getActivity();
        loadingDialog = new LoadingDialog(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentRegisterBinding.inflate(inflater, container,false);

        mAuth = FirebaseAuth.getInstance();

        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        loadView();
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = (LoginActivity) activity;
    }

    private void loadView() {
        if (binding != null) {
            binding.submitBtn.setOnClickListener(v -> {
                createAccount();
            });

            binding.verifyEmailBtn.setOnClickListener(v -> {
                sendEmailVerification();
            });
        }
    }

    private void createAccount() {
        Log.d(TAG, "createAccount: in process");
        if (!validateForm()) {
            return;
        }

        loadingDialog.show();

        // [START create_user_with_email]
        mAuth.createUserWithEmailAndPassword(
                binding.profileEmail.getText().toString(),
                binding.profilePass1.getText().toString())
                .addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "createUserWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            Toast.makeText(activity, "Authentication failed.",
                                    Toast.LENGTH_LONG).show();
                            updateUI(null);
                        }

                        // [START_EXCLUDE]
                        loadingDialog.dismiss();
                        // [END_EXCLUDE]
                    }
                });
        // [END create_user_with_email]
    }

    private boolean validateForm() {
        boolean valid = true;
        String msg = "";

        String email = binding.profileEmail.getText().toString();
        if (TextUtils.isEmpty(email)) {
            binding.profileEmail.setError("Requerido");
            msg = "Debes ingresar un correo";
            valid = false;
        } else {
            binding.profileEmail.setError(null);
        }

        String password = binding.profilePass1.getText().toString();
        if (TextUtils.isEmpty(password)) {
            binding.profilePass1.setError("Requerido");
            msg = msg.isEmpty() ? "Debes ingresar una contraseña" : msg;
            valid = false;
        } else {
            binding.profilePass1.setError(null);
        }

        String password2 = binding.profilePass2.getText().toString();
        if (TextUtils.isEmpty(password2)) {
            binding.profilePass2.setError("Requerido");
            msg = msg.isEmpty() ? "Debes repetir tu contraseña" : msg;
            valid = false;
        } else {
            binding.profilePass2.setError(null);
        }

        if (!TextUtils.isEmpty(password) && !TextUtils.isEmpty(password2) && !password.equals(password2)) {
            binding.profilePass1.setError("Requerido");
            binding.profilePass2.setError("Requerido");
            msg = msg.isEmpty() ? "Las contraseñas deben coincidir" : msg;
            valid = false;
        }

        if (!valid) {
            Toast.makeText(activity, msg, Toast.LENGTH_LONG).show();
        }

        return valid;
    }

    private void sendEmailVerification() {
        // Disable button
        binding.verifyEmailBtn.setEnabled(false);

        // Send verification email
        // [START send_email_verification]
        final FirebaseUser user = mAuth.getCurrentUser();
        user.sendEmailVerification()
                .addOnCompleteListener(activity, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        // [START_EXCLUDE]
                        // Re-enable button
                        binding.verifyEmailBtn.setEnabled(true);

                        if (task.isSuccessful()) {
                            Toast.makeText(activity,
                                    "Correo de verificacion enviado " + user.getEmail(),
                                    Toast.LENGTH_LONG).show();
                        } else {
                            Log.e(TAG, "sendEmailVerification", task.getException());
                            Toast.makeText(activity,
                                    "Failed to send verification email.",
                                    Toast.LENGTH_LONG).show();
                        }
                        // [END_EXCLUDE]
                    }
                });
        // [END send_email_verification]
    }

    private void updateUI(FirebaseUser user) {
        loadingDialog.dismiss();
        if (user != null) {
            Log.e(TAG, "Email: " + user.getEmail() + " Is verify: " + user.isEmailVerified() + " UID: " + user.getUid());
            if (user.isEmailVerified()) {
                binding.verifyEmailBtn.setVisibility(View.GONE);
            } else {
                Toast.makeText(activity, "Recuerda verificar tu correo", Toast.LENGTH_LONG).show();
                binding.verifyEmailBtn.setVisibility(View.VISIBLE);
            }
        }
    }
}
