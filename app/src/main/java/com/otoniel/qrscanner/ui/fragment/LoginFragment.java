package com.otoniel.qrscanner.ui.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthMultiFactorException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.MultiFactorResolver;
import com.otoniel.qrscanner.activity.LoginActivity;
import com.otoniel.qrscanner.activity.MainActivity;
import com.otoniel.qrscanner.databinding.FragmentLoginBinding;
import com.otoniel.qrscanner.ui.dialog.LoadingDialog;

public class LoginFragment extends Fragment {

    private static final String TAG = "LoginFragment";
    public static final int RESULT_NEEDS_MFA_SIGN_IN = 42;

    FragmentLoginBinding binding;

    private FirebaseAuth mAuth;
    private String mCustomToken;
    private LoginActivity activity;
    private LoadingDialog loadingDialog;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        activity = (LoginActivity) getActivity();
        loadingDialog = new LoadingDialog(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentLoginBinding.inflate(inflater, container,false);

        mAuth = FirebaseAuth.getInstance();

        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        loadView();
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = (LoginActivity) activity;
    }

    private void loadView() {
        if (binding != null) {
            binding.submitBtn.setOnClickListener(v -> {
                signIn();
            });
        }
    }

    private void signIn() {
        Log.d(TAG, "signIn: init");
        if (!validateForm()) {
            return;
        }

        loadingDialog.show();

        // [START sign_in_with_email]
        mAuth.signInWithEmailAndPassword(
                binding.profileEmail.getText().toString(),
                binding.profilePass1.getText().toString())
                .addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(activity, "Authentication failed.",
                                    Toast.LENGTH_LONG).show();
                            updateUI(null);
                            // [START_EXCLUDE]
                            checkForMultiFactorFailure(task.getException());
                            // [END_EXCLUDE]
                        }

                        // [START_EXCLUDE]
                        if (!task.isSuccessful()) {
                            //mBinding.status.setText(R.string.auth_failed);
                        }
                        loadingDialog.dismiss();
                        // [END_EXCLUDE]
                    }
                });
        // [END sign_in_with_email]
    }

    private boolean validateForm() {
        boolean valid = true;
        String msg = "";

        String email = binding.profileEmail.getText().toString();
        if (TextUtils.isEmpty(email)) {
            binding.profileEmail.setError("Requerido");
            msg = "Debes ingresar un correo";
            valid = false;
        } else {
            binding.profileEmail.setError(null);
        }

        String password = binding.profilePass1.getText().toString();
        if (TextUtils.isEmpty(password)) {
            binding.profilePass1.setError("Requerido");
            msg = msg.isEmpty() ? "Debes ingresar una contraseña" : msg;
            valid = false;
        } else {
            binding.profilePass1.setError(null);
        }

        if (!valid) {
            Toast.makeText(activity, msg, Toast.LENGTH_LONG).show();
        }

        return valid;
    }

    private void checkForMultiFactorFailure(Exception e) {
        // Multi-factor authentication with SMS is currently only available for
        // Google Cloud Identity Platform projects. For more information:
        // https://cloud.google.com/identity-platform/docs/android/mfa
        if (e instanceof FirebaseAuthMultiFactorException) {
            Log.w(TAG, "multiFactorFailure", e);
            Intent intent = new Intent();
            MultiFactorResolver resolver = ((FirebaseAuthMultiFactorException) e).getResolver();
            intent.putExtra("EXTRA_MFA_RESOLVER", resolver);
            activity.setResult(RESULT_NEEDS_MFA_SIGN_IN, intent);
            activity.finish();
        }
    }

    private void updateUI(FirebaseUser user) {
        if (user != null) {
            startActivity(new Intent(activity, MainActivity.class));
            activity.finish();
        } else {
            Toast.makeText(activity, "Inicio de sesion fallido", Toast.LENGTH_LONG).show();
        }
    }
}
