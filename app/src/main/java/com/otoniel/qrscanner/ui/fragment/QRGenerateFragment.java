package com.otoniel.qrscanner.ui.fragment;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.otoniel.qrscanner.activity.LoginActivity;
import com.otoniel.qrscanner.activity.MainActivity;
import com.otoniel.qrscanner.databinding.FragmentLoginBinding;
import com.otoniel.qrscanner.databinding.FragmentQrGenerateBinding;
import com.otoniel.qrscanner.ui.dialog.LoadingDialog;
import com.otoniel.qrscanner.utils.Utils;

import net.glxn.qrgen.android.QRCode;

import java.util.UUID;

public class QRGenerateFragment extends Fragment {

    private static final String TAG = "QRGenerateFragment";

    FragmentQrGenerateBinding binding;

    private FirebaseAuth mAuth;
    private FirebaseDatabase database;
    private String qrToken;
    private MainActivity activity;
    private LoadingDialog loadingDialog;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        activity = (MainActivity) getActivity();
        loadingDialog = new LoadingDialog(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentQrGenerateBinding.inflate(inflater, container,false);

        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();

        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        loadView();
        getQRCurrent();
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = (MainActivity) activity;
    }

    private void loadView() {
        if (binding != null) {
            binding.submitBtn.setOnClickListener(v -> {
                generateNewQR();
            });
        }
    }

    private void getQRCurrent() {
        loadingDialog.show();
        // [START write_message]
        // Write a message to the database
        DatabaseReference myRef = database.getReference("token_qr");

        //myRef.setValue("Hello, World!");
        // [END write_message]

        // [START read_message]
        // Read from the database
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                loadingDialog.dismiss();
                qrToken = dataSnapshot.getValue(String.class);
                Log.d(TAG, "Value is: " + qrToken);
                drawQR();
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                loadingDialog.dismiss();
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });
        // [END read_message]
    }

    private void generateNewQR() {
        loadingDialog.show();
        // [START write_message]
        // Write a message to the database
        DatabaseReference myRef = database.getReference("token_qr");

        //myRef.setValue(Utils.random());
        myRef.setValue(UUID.randomUUID().toString());

        loadingDialog.dismiss();
    }

    private void drawQR() {
        Bitmap bitmap = QRCode.from(qrToken).bitmap();

        binding.qrImg.setImageBitmap(bitmap);
    }
}
