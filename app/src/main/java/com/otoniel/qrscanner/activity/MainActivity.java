package com.otoniel.qrscanner.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.otoniel.qrscanner.R;
import com.otoniel.qrscanner.databinding.ActivityMainBinding;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    ActivityMainBinding binding;
    NavController controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        controller = Navigation.findNavController(this, R.id.nav_host_fragment);

        NavigationUI.setupWithNavController(binding.navView, controller);

        initToolbar();

        verifyPermission();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_appbar_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.exit) {
            FirebaseAuth.getInstance().signOut();
            finish();
        }
        Log.e("ID MENU", item.getItemId() + "");

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 100:
                //acción o método a realizar.
                if (!(grantResults.length > 0 && grantResults[0] == PERMISSION_GRANTED)) {
                    Toast.makeText(getApplicationContext(), "Debe conceder los permisos para poder usar la aplicación en toda su capacidad", Toast.LENGTH_LONG).show();
                    finish();
                }
                break;
        }
    }

    private void initToolbar() {
        setSupportActionBar(binding.toolbarPrimary);
    }

    private void verifyPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int permsRequestCode = 100;
            String[] perms = {
                    Manifest.permission.CAMERA,
                    Manifest.permission.VIBRATE
            };
            int cameaPermission = checkSelfPermission(Manifest.permission.CAMERA);
            int vibratePermission = checkSelfPermission(Manifest.permission.VIBRATE);

            if (cameaPermission == PackageManager.PERMISSION_GRANTED &&
                    vibratePermission == PackageManager.PERMISSION_GRANTED) {
                //se realiza metodo si es necesario...

            } else {
                requestPermissions(perms, permsRequestCode);
            }
        }
        else {

        }
    }
}