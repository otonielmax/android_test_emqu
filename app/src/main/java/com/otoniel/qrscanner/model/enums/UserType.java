package com.otoniel.qrscanner.model.enums;

public enum UserType {
    CLIENT,
    STORE
}
